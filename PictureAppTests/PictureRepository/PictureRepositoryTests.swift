//
//  PictureRepositoryTests.swift
//  PictureAppTests
//
//  Created by Jean Torres on 20/02/2022.
//

import XCTest
@testable import PictureApp

class PictureRepositoryTests: XCTestCase {

    var subject: PictureRepositoryProtocol.Type = PictureRepository.self
    
    var token = "86a58c98b5095e4e069222d9caaf57e27a4c0028772b5a76e4f42ff9a53ab563"
    
    let urlSession = MockUrlSession.shared
    
    override func setUp() {
        super.setUp()
        subject = PictureRepository.self
        subject.urlSession = urlSession
    }
    
    func testGetAllPictureRoute() async throws {
        _ = try await subject.getPictures(page: 2)
        XCTAssertEqual(urlSession.request?.httpMethod, "GET")
        XCTAssertEqual(urlSession.request?.value(forHTTPHeaderField: "Authorization"), "Client-ID \(token)")
        XCTAssertEqual(urlSession.request?.url?.absoluteString, "https://api.unsplash.com/photos?page=2")
    }
    
    func testGetAllPictureObject() async throws {
        let pictures = try await subject.getPictures(page: 1)
        XCTAssertEqual(pictures.count, 3)
        XCTAssertEqual(pictures.first?.id, "lFmuWU0tv4M")
        XCTAssertEqual(pictures.first?.description, "White page with color chart for your ideas!")
        XCTAssertEqual(pictures.first?.user?.name, "Keila Hötzel")
        XCTAssertEqual(pictures.first?.user?.location, "Bayern, Germany")
        XCTAssertEqual(pictures.first?.urls?.small, "https://images.unsplash.com/photo-1536329583941-14287ec6fc4e?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3Njc0MHwxfDF8YWxsfDF8fHx8fHwyfHwxNjQ1NDI4MTU3&ixlib=rb-1.2.1&q=80&w=400")
        
        XCTAssertEqual(pictures[2].user?.location, nil)
    }
    
    func testGetAllPictureAndReceiveError() async throws {
        subject.urlSession = MockUrlSessionWithError.shared
        do {
            _ = try await subject.getPictures(page: 1)
        } catch {
            XCTAssertEqual(error.toNetworkError, NetworkError.server)
        }
    }
    
    func testGetPictureDetailsRoute() async throws {
        _ = try await subject.getDetails(pictureId: "lFmuWU0tv4M")
        XCTAssertEqual(urlSession.request?.httpMethod, "GET")
        XCTAssertEqual(urlSession.request?.value(forHTTPHeaderField: "Authorization"), "Client-ID \(token)")
        XCTAssertEqual(urlSession.request?.url?.absoluteString, "https://api.unsplash.com/photos/lFmuWU0tv4M")
    }
    
    func testGetPictureDetailsAndReceiveError() async throws {
        subject.urlSession = MockUrlSessionWithError.shared
        do {
            _ = try await subject.getDetails(pictureId: "lFmuWU0tv4M")
        } catch {
            XCTAssertEqual(error.toNetworkError, NetworkError.server)
        }
    }
    
    func testGetPictureDetailsObject() async throws {
        let picture = try await subject.getDetails(pictureId: "HTGOE0_whew")
        XCTAssertEqual(picture.id, "HTGOE0_whew")
        XCTAssertEqual(picture.description, nil)
        XCTAssertEqual(picture.user?.name, "Gaspar Zaldo")
        XCTAssertEqual(picture.user?.location, "Cordoba")
        XCTAssertEqual(picture.urls?.small, "https://images.unsplash.com/photo-1645389692136-4b344532362f?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3Njc0MHwwfDF8YWxsfHx8fHx8fHx8MTY0NTQzMDA5Mw&ixlib=rb-1.2.1&q=80&w=400")
        XCTAssertEqual(picture.urls?.regular, "https://images.unsplash.com/photo-1645389692136-4b344532362f?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3Njc0MHwwfDF8YWxsfHx8fHx8fHx8MTY0NTQzMDA5Mw&ixlib=rb-1.2.1&q=80&w=1080")
        
        XCTAssertEqual(picture.exif?.make, "NIKON CORPORATION")
        XCTAssertEqual(picture.exif?.model, "NIKON D500")
        XCTAssertEqual(picture.exif?.name, "NIKON CORPORATION, NIKON D500")
        XCTAssertEqual(picture.exif?.exposure_time, "1/100")
        XCTAssertEqual(picture.exif?.aperture, "2.2")
        XCTAssertEqual(picture.exif?.focal_length, "35.0")
        XCTAssertEqual(picture.exif?.iso, 400)
    }
}
