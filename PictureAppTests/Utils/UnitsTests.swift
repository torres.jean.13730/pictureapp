//
//  UnitsTests.swift
//  PictureAppTests
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation
@testable import PictureApp

func loadData(_ fileName:String) -> Data? {
    do {
        if let file = Bundle(for: PictureAppTests.self).url(forResource: fileName, withExtension: "json") {
            let data = try Data(contentsOf: file)
            return data
        } else {
            print("no file")
        }
    } catch {
        print(error.localizedDescription)
        return nil
    }
    return nil
}

func retriveExistingFile(forUrl url: URL?) -> Data? {
    guard let urlString = url?.absoluteString else {
        return nil
    }
    
    if let uri = urlString.components(separatedBy: Constants.apiHost).last {
        let fileName = uri.replacingOccurrences(of: "/", with: ":")
        return loadData(fileName)
    }
    return nil
}

func getObject<T:Codable>(_ fileName: String) -> T? {
    do {
        guard let data = loadData(fileName) else { return nil }
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        return nil
    }
}

func getObjectArray<T:Codable>(_ fileName: String) -> [T] {
    do {
        guard let data = loadData(fileName) else { return [T]() }
        let decoder = JSONDecoder()
        return try decoder.decode([T].self, from: data)
    } catch {
        return [T]()
    }
}
