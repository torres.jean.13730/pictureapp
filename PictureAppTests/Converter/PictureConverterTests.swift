//
//  PictureConverterTests.swift
//  PictureAppTests
//
//  Created by Jean Torres on 22/02/2022.
//

import XCTest
@testable import PictureApp

class PictureConverterTests: XCTestCase {

    var subject: PictureConverterProtocol.Type = PictureConverter.self
    
    override func setUp() {
        super.setUp()
    }
    
    func testConverterPicturesToListPictureUIData()  {
        let output = PicturesFixtures.get()
        let pictures = subject.convertPicturesToListPictureUIData(output)
        XCTAssertEqual(pictures.count, 3)
        XCTAssertEqual(pictures.first?.id, "lFmuWU0tv4M")
        XCTAssertEqual(pictures.first?.describe, "White page with color chart for your ideas!")
        XCTAssertEqual(pictures.first?.author, "Keila Hötzel")
        XCTAssertEqual(pictures.first?.location, "Bayern, Germany")
        XCTAssertEqual(pictures.first?.imageUrl, "https://images.unsplash.com/photo-1536329583941-14287ec6fc4e?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3Njc0MHwxfDF8YWxsfDF8fHx8fHwyfHwxNjQ1NDI4MTU3&ixlib=rb-1.2.1&q=80&w=400")
    }

}
