//
//  PictureDetailsConverterTests.swift
//  PictureAppTests
//
//  Created by Jean Torres on 24/02/2022.
//

import XCTest
@testable import PictureApp

class PictureDetailsConverterTests: XCTestCase {

    var subject: PictureDetailsConverterProtocol.Type = PictureDetailsConverter.self
    
    override func setUp() {
        super.setUp()
    }
    
    func testConverterPicturesToListPictureUIData()  {
        let output = PictureDetailsFixtures.get()
        let picture = subject.convertPictureToPictureDetailsUIData(output)
        XCTAssertEqual(picture.imageUrl, "https://images.unsplash.com/photo-1645389692136-4b344532362f?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3Njc0MHwwfDF8YWxsfHx8fHx8fHx8MTY0NTQzMDA5Mw&ixlib=rb-1.2.1&q=80&w=1080")
        XCTAssertEqual(picture.exif, ["aperture": "2.2",
                                       "make": "NIKON CORPORATION",
                                       "iso": "400",
                                       "model": "NIKON D500",
                                       "name": "NIKON CORPORATION, NIKON D500",
                                       "exposure_time": "1/100",
                                       "focal_length": "35.0"])
    }
}
