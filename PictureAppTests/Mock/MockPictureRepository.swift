//
//  MockPictureRepository.swift
//  PictureAppTests
//
//  Created by Jean Torres on 24/02/2022.
//

import Foundation
import XCTest
@testable import PictureApp

class MockPictureRepository: PictureRepositoryProtocol {

    static let shared = MockPictureRepository()

    var invocations = [Invocation]()
    
    static var urlSession: URLSessionProtocol = URLSession.shared

    enum Invocation: Equatable {
        case getPictures(startPage: Int)
        case getDetails(pictureId: String)
    }

    static func verify(_ expected: [Invocation]) {
        XCTAssertEqual(expected.count, 1)
        XCTAssertEqual(MockPictureRepository.shared.invocations.count, 1)
        XCTAssertEqual(MockPictureRepository.shared.invocations, expected)
        MockPictureRepository.shared.invocations.removeAll()
    }
    
    
    static func getPictures(page: Int) async throws -> [Picture] {
        MockPictureRepository.shared.invocations.append(contentsOf: [.getPictures(startPage: page)])
        return getObjectArray(":photos?page=1")
    }
    
    static func getDetails(pictureId: String) async throws -> Picture {
        MockPictureRepository.shared.invocations.append(contentsOf: [.getDetails(pictureId: pictureId)])
        return getObject(":photos:\(pictureId)") ?? Picture()
    }
}
