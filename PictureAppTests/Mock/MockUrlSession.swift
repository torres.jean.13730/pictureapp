//
//  MockUrlSession.swift
//  PictureAppTests
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation
@testable import PictureApp

class MockUrlSession: URLSessionProtocol {
    
    static let shared = MockUrlSession()
    
    var request: URLRequest?
    
    var data: Data = Data()
    
    func data(for request: URLRequest, delegate: URLSessionTaskDelegate?) async throws -> (Data, URLResponse) {
        self.request = request
        let urlResponse = HTTPURLResponse(url: request.url!,
                                          statusCode: 200,
                                          httpVersion: "1.0",
                                          headerFields: nil)!
        
        if let data = retriveExistingFile(forUrl: request.url) {
            self.data = data
        }
        
        return (data, urlResponse)
    }
}

class MockUrlSessionWithError: URLSessionProtocol {
    
    static let shared = MockUrlSessionWithError()
    
    var request: URLRequest?
    
    var data: Data = Data()
    
    func data(for request: URLRequest, delegate: URLSessionTaskDelegate?) async throws -> (Data, URLResponse) {
        let urlResponse = HTTPURLResponse(url: request.url!,
                                          statusCode: 403,
                                          httpVersion: "1.0",
                                          headerFields: nil)!
        
        return (data, urlResponse)
    }
}
