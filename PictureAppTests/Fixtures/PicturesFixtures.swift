//
//  PicturesFixtures.swift
//  PictureAppTests
//
//  Created by Jean Torres on 22/02/2022.
//

import Foundation
@testable import PictureApp

class PicturesFixtures {
    class func get() -> [Picture] {
        guard let data = loadData(":photos?page=1") else {
            return []
        }
        let jsonDecoder = JSONDecoder()
        do {
            return try jsonDecoder.decode([Picture].self, from: data)
        } catch {
            print("can't load json")
        }
        
        return []
    }
}
