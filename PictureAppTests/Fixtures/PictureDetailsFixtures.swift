//
//  PictureDetailsFixtures.swift
//  PictureAppTests
//
//  Created by Jean Torres on 24/02/2022.
//

import Foundation
@testable import PictureApp

class PictureDetailsFixtures {
    class func get() -> Picture {
        guard let data = loadData(":photos:HTGOE0_whew") else {
            return Picture()
        }
        let jsonDecoder = JSONDecoder()
        do {
            return try jsonDecoder.decode(Picture.self, from: data)
        } catch {
            print("can't load json")
        }
        
        return Picture()
    }
}
