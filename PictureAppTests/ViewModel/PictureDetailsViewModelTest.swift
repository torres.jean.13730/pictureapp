//
//  PictureDetailsViewModelTest.swift
//  PictureAppTests
//
//  Created by Jean Torres on 24/02/2022.
//

import XCTest
@testable import PictureApp

class PictureDetailsViewModelTest: XCTestCase {

    var subject: PictureDetailsViewModelProtocol!
    
    override func setUp() {
        super.setUp()
        let repository = MockPictureRepository.self
        subject = PictureDetailsViewModel(repository:repository, pictureId: "HTGOE0_whew")
        MockPictureRepository.shared.invocations.removeAll()
    }
    
    func testToGetPictureDetails()  {
        let expectation = XCTestExpectation(description: "get Pictures")
        var numberState = 0
        subject.stateDidChanged = { state in
            print(state)
            switch numberState {
            case 0:
                XCTAssertEqual(state, .fetching)
            case 1:
                XCTAssertEqual(state, .fetched)
                XCTAssertEqual(self.subject.uidata?.imageUrl, "https://images.unsplash.com/photo-1645389692136-4b344532362f?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3Njc0MHwwfDF8YWxsfHx8fHx8fHx8MTY0NTQzMDA5Mw&ixlib=rb-1.2.1&q=80&w=1080")
                XCTAssertEqual(self.subject.uidata?.exif, ["aperture": "2.2",
                                                           "make": "NIKON CORPORATION",
                                                           "iso": "400",
                                                           "model": "NIKON D500",
                                                           "name": "NIKON CORPORATION, NIKON D500",
                                                           "exposure_time": "1/100",
                                                           "focal_length": "35.0"])
                MockPictureRepository.verify([.getDetails(pictureId: "HTGOE0_whew")])
                expectation.fulfill()
            default:
                XCTFail()
            }
            numberState += 1
        }
        XCTAssertNil(self.subject.uidata)
        subject.getPictureDetails()
        wait(for: [expectation], timeout: 5)
    }

}
