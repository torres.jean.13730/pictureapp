//
//  PictureViewModelTests.swift
//  PictureAppTests
//
//  Created by Jean Torres on 22/02/2022.
//

import XCTest
@testable import PictureApp

class PictureViewModelTests: XCTestCase {

    var subject: PictureViewModelProtocol!
    
    override func setUp() {
        super.setUp()
        let repository = MockPictureRepository.self
        subject = PictureViewModel(repository:repository)
        MockPictureRepository.shared.invocations.removeAll()
    }
    
    func testToGetPictures()  {
        let expectation = XCTestExpectation(description: "get Pictures")
        var numberState = 0
        subject.stateDidChanged = { state in
            print(state)
            switch numberState {
            case 0:
                XCTAssertEqual(state, .fetching)
            case 1:
                XCTAssertEqual(state, .fetched)
                XCTAssertEqual(self.subject.page, 1)
                XCTAssertEqual(self.subject.uidata.count, 3)
                MockPictureRepository.verify([.getPictures(startPage: 1)])
                expectation.fulfill()
            default:
                XCTFail()
            }
            numberState += 1
        }
        XCTAssertEqual(self.subject.uidata.count, 0)
        subject.getPictures()
        wait(for: [expectation], timeout: 5)
    }

    func testToLoadMore()  {
        let expectation = XCTestExpectation(description: "Load more picture")
        var numberState = 0
        subject.stateDidChanged = { state in
            switch numberState {
            case 0:
                XCTAssertEqual(state, .fetching)
            case 1:
                XCTAssertEqual(state, .fetched)
                XCTAssertEqual(self.subject.page, 2)
                MockPictureRepository.verify([.getPictures(startPage: 2)])
                expectation.fulfill()
            default:
                XCTFail()
            }
            numberState += 1
        }
        XCTAssertEqual(self.subject.page, 1)
        subject.loadMore()
        wait(for: [expectation], timeout: 5)
    }
    
    func testToRefreshData()  {
        let expectation = XCTestExpectation(description: "Refresh picture")
        subject.page = 4
        var numberState = 0
        subject.stateDidChanged = { state in
            switch numberState {
            case 0:
                XCTAssertEqual(state, .fetching)
            case 1:
                XCTAssertEqual(state, .fetched)
                XCTAssertEqual(self.subject.page, 1)
                MockPictureRepository.verify([.getPictures(startPage: 1)])
                expectation.fulfill()
            default:
                XCTFail()
            }
            numberState += 1
        }
        
        XCTAssertEqual(self.subject.page, 4)
        subject.refresh()
        wait(for: [expectation], timeout: 5)
    }
}
