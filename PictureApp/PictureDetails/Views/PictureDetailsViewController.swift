//
//  PictureDetailsViewController.swift
//  PictureApp
//
//  Created by Jean Torres on 23/02/2022.
//

import Foundation
import UIKit


class PictureDetailsViewController: UIViewController {

    // MARK: - variables
    var viewModel: PictureDetailsViewModelProtocol? {
        didSet {
            self.viewModel?.stateDidChanged = handleState
            self.viewModel?.getPictureDetails()
        }
    }
    
    var coordinator: PictureDetailsCoordinator?
    
    var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.alpha = 0
        return stackView
    }()
    
    let contentExif: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        return view
    }()
    
    private var exifIsShow = false
      
    // MARK: - init
    public init(_ coder: NSCoder? = nil) {

        if let coder = coder {
            super.init(coder: coder)!
        } else {
            super.init(nibName: nil, bundle: nil)
        }
        self.setupView()
        self.setupConstraint()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
        self.setupConstraint()
    }

    // MARK: - setupView
    func setupView() {
        setBackgroundNavBar(UIColor.black.withAlphaComponent(0.3))
        
        let closeButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.stop,
                                         target: self,
                                         action: #selector(dismissView))
        closeButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = closeButton

        stackView.alpha = exifIsShow ? 1 : 0
        
        contentExif.addSubview(stackView)
        self.view.addSubview(imageView)
        self.view.addSubview(contentExif)
    }
    
    func setupInfoExif() {
        stackView.subviews.forEach({$0.removeFromSuperview()})
        
        if let exifs = self.viewModel?.uidata?.exif, exifs.count > 0 {
            displayExifButton(true)
            let exifSorted = exifs.sorted(by: {$0.key < $1.key})
            exifSorted.forEach({ exif in
                let label = UILabel()
                label.font = UIFont.systemFont(ofSize: 14)
                let text = exif.key.localized + ": " + exif.value
                label.attributedText = text.getAtributeString([exif.key.localized + ":"], color: .white, fontBold: UIFont.boldSystemFont(ofSize: 16))
                label.textColor = .white
                label.textAlignment = .center
                stackView.addArrangedSubview(label)
            })
        } else {
            displayExifButton(false)
        }
        view.layoutIfNeeded()
    }
    
    // MARK: - setupConstraint
    func setupConstraint() {
        imageView.fillSuperView()
        contentExif.anchor(top: view.bottomAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: .init(top: 0, left: 0, bottom: 0, right: 0))

        stackView.anchor(top: contentExif.topAnchor,
                         leading: contentExif.leadingAnchor,
                         bottom: contentExif.bottomAnchor,
                         trailing: contentExif.trailingAnchor,
                         padding:.init(top: 25, left: 0, bottom: 25, right: 0))
    }
}

// MARK: - actions
extension PictureDetailsViewController {
    
    @objc func dismissView() {
        coordinator?.navigationController.popViewController(animated: true)
    }

    @objc func showMoreInfo() {
        let showDown = CGPoint(x: contentExif.center.x ,y: contentExif.center.y + contentExif.bounds.height)
        let showUp = CGPoint(x: contentExif.center.x ,y: contentExif.center.y - contentExif.bounds.height)
        UIView.animate(withDuration: 0.2) {
            self.contentExif.center = self.exifIsShow ? showDown : showUp
            self.stackView.alpha = self.exifIsShow ? 0 : 1
        }
        exifIsShow = !exifIsShow
        
    }
    
    func displayExifButton(_ display:Bool) {
        guard display else {self.navigationItem.rightBarButtonItem = nil; return}
        let infoButton = UIBarButtonItem(title: "info".localized,
                                         style: .done,
                                         target: self,
                                         action: #selector(showMoreInfo))
        infoButton.tintColor = .white
        self.navigationItem.rightBarButtonItem = infoButton
    }
}

// MARK: - handleState
extension PictureDetailsViewController {
    private func handleState(_ state: ViewModelStateEnum) {
        switch state {
        case .fetching:
            break
        case .fetched:
            if let urlString = self.viewModel?.uidata?.imageUrl,
               let url = URL(string: urlString) {
                self.imageView.load(url: url,
                                    placeholder: "placeholder-image".imageFromName,
                                    cache: nil)
            }
            setupInfoExif()
        case .error(error: let error):
        #if DEBUG
            print("###### Error Get Picture Details #######")
            print(error.localizedDescription)
        #endif
            self.displayError(message: error.localizedDescription)
        default:
            break
        }
    }
}
