//
//  PictureDetailsConverter.swift
//  PictureApp
//
//  Created by Jean Torres on 23/02/2022.
//

import Foundation

protocol PictureDetailsConverterProtocol {
    static func convertPictureToPictureDetailsUIData(_ output:Picture) -> PictureDetailsUIData
}

class PictureDetailsConverter: PictureDetailsConverterProtocol {
    
    /// convert a Pictures output to Picture Details UIData
    /// - Parameter output: Picture
    /// - Returns: PictureDetailsUIData
    static func convertPictureToPictureDetailsUIData(_ output:Picture) -> PictureDetailsUIData {
        var uidata = PictureDetailsUIData()
        uidata.imageUrl = output.urls?.regular

        if let exif = output.exif {
            uidata.exif = convertExifToDictionnary(exif)
        }
        
        return uidata
    }
    
    /// convert a Exif to Dictionnary
    /// - Parameter output: Picture
    /// - Returns: PictureDetailsUIData
    private static func convertExifToDictionnary(_ exif: Exif) -> [String:String]{
        var result:[String:String] = [:]
        if let make = exif.make { result["make"] = make }
        if let model = exif.model {result["model"] = model }
        if let name = exif.name {result["name"] = name }
        if let exposure_time = exif.exposure_time {result["exposure_time"] = exposure_time}
        if let aperture = exif.aperture {result["aperture"] = aperture}
        if let focal_length = exif.focal_length {result["focal_length"] = focal_length}
        if let iso = exif.iso {result["iso"] = iso.description }
        return result
    }
}
