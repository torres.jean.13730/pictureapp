//
//  PictureDetailsAssembler.swift
//  PictureApp
//
//  Created by Jean Torres on 23/02/2022.
//

import Foundation
import UIKit

class PictureDetailsAssembler {
    class func assemble(pictureId: String) -> PictureDetailsViewController {
        let viewController = PictureDetailsViewController()
        viewController.viewModel = PictureDetailsViewModel(repository: PictureRepository.self,
                                                           pictureId: pictureId)
        return viewController
    }
}
