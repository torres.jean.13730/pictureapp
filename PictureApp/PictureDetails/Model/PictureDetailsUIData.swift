//
//  PictureDetailsUIData.swift
//  PictureApp
//
//  Created by Jean Torres on 23/02/2022.
//

import Foundation

struct PictureDetailsUIData {
    
    var imageUrl:String?
    
    var exif: [String:String]?

}
