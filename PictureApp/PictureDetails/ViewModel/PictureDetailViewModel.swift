//
//  PictureDetailViewModel.swift
//  PictureApp
//
//  Created by Jean Torres on 23/02/2022.
//

import Foundation

protocol PictureDetailsViewModelProtocol {
    
    func getPictureDetails()
    
    var uidata: PictureDetailsUIData? { get set }
    
    var stateDidChanged: ((ViewModelStateEnum) -> Void)? { get set }
    
}

class PictureDetailsViewModel: PictureDetailsViewModelProtocol {
    
    // MARK: - variables
    private var state: ViewModelStateEnum = .unknown {
        didSet {
            DispatchQueue.main.async {
                self.stateDidChanged?(self.state)
            }
        }
    }
    
    var stateDidChanged: ((ViewModelStateEnum) -> Void)?
    
    private let pictureId: String
    
    var uidata: PictureDetailsUIData?
    
    var repository: PictureRepositoryProtocol.Type
    
    var converter: PictureDetailsConverterProtocol.Type = PictureDetailsConverter.self
    
    // MARK: - init
    init(repository: PictureRepositoryProtocol.Type, pictureId: String) {
        self.repository = repository
        self.pictureId = pictureId
    }
    
    // MARK: - Method
    func getPictureDetails() {
        Task {
            self.state = .fetching
            do {
                let result = try await repository.getDetails(pictureId: self.pictureId)
                self.uidata = converter.convertPictureToPictureDetailsUIData(result)
                self.state = .fetched
            } catch {
                self.state = .error(error: error.toNetworkError)
            }
        }
    }
}
