//
//  PictureDetailsCoordinator.swift
//  PictureApp
//
//  Created by Jean Torres on 23/02/2022.
//

import Foundation
import UIKit

protocol PicturesDetailsCoordinatorProtocol: Coordinator {
    var pictureId: String {get set}
}

class PictureDetailsCoordinator: PicturesDetailsCoordinatorProtocol {
        
    var navigationController: UINavigationController
    
    var pictureId: String
    
    required init(navigationController: UINavigationController, pictureId: String) {
        self.navigationController = navigationController
        self.pictureId = pictureId
    }
    
    func start() {
        let viewController = PictureDetailsAssembler.assemble(pictureId: pictureId)
        viewController.coordinator = self
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    
}
