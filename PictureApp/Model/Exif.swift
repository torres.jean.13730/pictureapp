//
//  Exif.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation

struct Exif: Codable {
    
    var make: String?
    
    var model: String?
    
    var name: String?
    
    var exposure_time: String?
    
    var aperture: String?
    
    var focal_length: String?
    
    var iso: Int?
    
}
