//
//  URLs.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation

struct URLs: Codable {
    
    var small: String?
    
    var regular: String?
}
