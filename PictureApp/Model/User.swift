//
//  User.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation

struct User: Codable {
    
    var id: String?
    
    var name: String?
    
    var location: String?
    
}
