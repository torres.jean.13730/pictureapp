//
//  Picture.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation

struct Picture: Codable {
    
    var id: String?
    
    var description: String?
    
    var user: User?
    
    var urls: URLs?
    
    var exif: Exif?
    
}



