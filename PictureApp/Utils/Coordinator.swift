//
//  Coordinator.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    
    var navigationController: UINavigationController { get set }
    
    func start()
}
