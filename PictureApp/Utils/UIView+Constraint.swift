//
//  UIView+Constraint.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation
import UIKit

extension UIView {
    
    public var safeAreaFrame: CGRect {
        if #available(iOS 11, *) {
            return safeAreaLayoutGuide.layoutFrame
        }
        return bounds
    }
    
    /// Methode: view take same constraint to superview
    func fillSuperView(withSafeArea:Bool = false) {
        let margins = superview?.safeAreaLayoutGuide
        anchor(top: withSafeArea ? margins?.topAnchor : superview?.topAnchor,
               leading: withSafeArea ? margins?.leadingAnchor : superview?.leadingAnchor,
               bottom: withSafeArea ? margins?.bottomAnchor : superview?.bottomAnchor,
               trailing: withSafeArea ? margins?.trailingAnchor : superview?.trailingAnchor)
        
    }
    
    /// Methode: view add constraint for with == height
    func anchorHeightEgalWith() {
        widthAnchor.constraint(equalTo: heightAnchor).isActive = true
    }
    
    /// Methode for create constraint with LayoutAnchor
    /// - Parameters:
    ///   - top: constraint for top
    ///   - leading: constraint for leading
    ///   - bottom: constraint for bottom
    ///   - trailing: constraint for trailing
    ///   - bottomLess: constraint for bottom equal or less
    ///   - padding: add padding
    ///   - size: add constraint size
    func anchor(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, trailing: NSLayoutXAxisAnchor?, bottomLess: NSLayoutYAxisAnchor? = nil, padding: UIEdgeInsets = .zero, size: CGSize = .zero) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom).isActive = true
        }
        
        if let bottomLess = bottomLess {
            bottomAnchor.constraint(lessThanOrEqualTo: bottomLess, constant: -padding.bottom).isActive = true
        }
        
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: -padding.right).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
}
