//
//  UICollectionView+Extension.swift
//  PictureApp
//
//  Created by Jean Torres on 25/02/2022.
//

import Foundation
import UIKit
extension UICollectionView {

    /// Set Empty View Message
    /// - Parameter message: message to show in emptyVIew (String)
    func setEmptyView(_ message: String) {
        let messageError = UILabel()
        messageError.text = message
        messageError.textColor = .black
        messageError.numberOfLines = 0;
        messageError.textAlignment = .center;
        messageError.font = UIFont.systemFont(ofSize: 17)
        messageError.sizeToFit()
        messageError.fillSuperView()
        self.backgroundView = messageError;
    }

    func restore() {
        self.backgroundView = nil
    }
}
