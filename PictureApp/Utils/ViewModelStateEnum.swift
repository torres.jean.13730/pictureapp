//
//  ViewModelStateEnum.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation

enum ViewModelStateEnum: Equatable {
    case fetching
    case fetched
    case error(error:NetworkError)
    case unknown
}
