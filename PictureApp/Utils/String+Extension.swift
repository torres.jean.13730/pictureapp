//
//  String+Extension.swift
//  PictureApp
//
//  Created by Jean Torres on 24/02/2022.
//

import Foundation
import UIKit

extension String {
    
    /// Create a atribute string with bold word
    /// - Parameters:
    ///   - textBold: text that we want to bold
    ///   - color: color for bold text
    ///   - fontBold: font for bold text
    /// - Returns: NSAttributedString
    func getAtributeString(_ textBold: [String], color: UIColor, fontBold: UIFont?) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        for string in textBold {
            let ranges = self.ranges(of: string)
            ranges.forEach({ range in
                attributedString.addAttribute(NSAttributedString.Key.font, value: fontBold as Any, range: NSRange(range, in: self))
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSRange(range, in: self))
            })
        }
        return attributedString
    }
    
    /// methode for get range of substring in the string
    /// - Parameters:
    ///   - substring: text that we want know the range in the string
    /// - Returns: [Range<Index>]
    fileprivate func ranges(of substring: String) -> [Range<Index>] {
        var ranges: [Range<Index>] = []
        while let range = range(of: substring, options: [], range: (ranges.last?.upperBound ?? self.startIndex)..<self.endIndex, locale: nil) {
            ranges.append(range)
        }
        return ranges
    }
}

extension String {
    /// return trad from Localizables
    var localized: String {
        let tableName = "Localizables"
        let langue = Locale.current.languageCode ?? "en-US"
        let path = Bundle.main.path(forResource: langue, ofType: "lproj")!
        let bundle = Bundle(path: path)!
        return bundle.localizedString(forKey: self, value: self, table: tableName)
    }
}


extension String {
    /// return image from Assets
    var imageFromName: UIImage? {
        return UIImage(named: self)
    }
}
