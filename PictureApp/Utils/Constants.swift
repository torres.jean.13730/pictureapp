//
//  Constants.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation
import UIKit

struct Constants {
    
    static let apiHost = "https://api.unsplash.com"
    
    static var token = "86a58c98b5095e4e069222d9caaf57e27a4c0028772b5a76e4f42ff9a53ab563"
    
    static var navigationController: UINavigationController = UINavigationController()
    
}
