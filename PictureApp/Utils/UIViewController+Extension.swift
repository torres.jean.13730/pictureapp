//
//  UIViewController+Extension.swift
//  PictureApp
//
//  Created by Jean Torres on 24/02/2022.
//

import Foundation
import UIKit

extension UIViewController {
    
    /// Methode for set background NavigationBar
    /// - Parameter backgroundColor: background color for navigationBar
    func setBackgroundNavBar(_ backgroundColor: UIColor) {
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationBarAppearance.backgroundColor = backgroundColor
        navigationBarAppearance.backgroundEffect = nil
        navigationItem.scrollEdgeAppearance = navigationBarAppearance
        navigationItem.standardAppearance = navigationBarAppearance
        navigationItem.compactAppearance = navigationBarAppearance
        navigationController?.setNeedsStatusBarAppearanceUpdate()
    }
    
    
    /// Display error in alert
    /// - Parameter message: message error to display
    func displayError(message:String) {
        let alert = UIAlertController(title: "error".localized, message: message.localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
