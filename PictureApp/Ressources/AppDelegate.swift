//
//  AppDelegate.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Config.setupAppearance()
        window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
        self.window?.rootViewController = Constants.navigationController
        let coordinator = PicturesCoordinator(navigationController: Constants.navigationController)
        coordinator.start()
        return true
    }


}

