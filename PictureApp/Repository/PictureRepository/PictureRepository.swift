//
//  PictureRepository.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation

protocol PictureRepositoryProtocol: ApiProtocol {
    
    static func getPictures(page:Int) async throws -> [Picture]
    
    static func getDetails(pictureId: String) async throws -> Picture
    
}

class PictureRepository: PictureRepositoryProtocol {
    
    static var urlSession: URLSessionProtocol = URLSession.shared
    
    /// return a pictures list
    ///  endpoint: - /photos?page=1
    /// - Parameter page: Int page to return
    /// - Returns: Picture list [Picture]
    class func getPictures(page:Int) async throws -> [Picture] {
        let url = URL(string: Router.getPictures(params: ["page":page]).route)
        var request = URLRequest(url:url!)
        request.httpMethod = "GET"
        request.setValue("Client-ID \(Constants.token)", forHTTPHeaderField: "Authorization")
        request.showCurl()
        
        let (data, response) = try await urlSession.data(for: request, delegate: nil)
        
        guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
            throw NetworkError.server
        }

        do {
            let decoder = JSONDecoder()
            let objects = try decoder.decode([Picture].self, from: data)
            return objects
        } catch {
            throw NetworkError.invalidJson
        }
    }
    
    /// return a Picture details
    /// - Parameter pictureId: String
    /// - Returns: return a picture details (Picture)
    class func getDetails(pictureId: String) async throws -> Picture {
        let url = URL(string: Router.getPictureDetails(pictureId: pictureId).route)
        var request = URLRequest(url:url!)
        request.httpMethod = "GET"
        request.setValue("Client-ID \(Constants.token)", forHTTPHeaderField: "Authorization")
        request.showCurl()
        let (data, response) = try await urlSession.data(for: request, delegate: nil)
        
        guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
            throw NetworkError.server
        }
        
        do {
            let decoder = JSONDecoder()
            let object = try decoder.decode(Picture.self, from: data)
            return object
        } catch {
            throw NetworkError.invalidJson
        }
    }
}



