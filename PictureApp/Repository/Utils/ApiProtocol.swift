//
//  ApiProtocol.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation

protocol ApiProtocol {
    static var urlSession: URLSessionProtocol {get set}
}
