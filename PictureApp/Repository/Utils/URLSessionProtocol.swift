//
//  URLSessionProtocol.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation

protocol URLSessionProtocol {
    func data(for request: URLRequest, delegate: URLSessionTaskDelegate?) async throws -> (Data, URLResponse)
}
