//
//  Router.swift
//  PictureApp
//
//  Created by Jean Torres on 24/02/2022.
//

import Foundation

enum Router {
    case getPictures(params:[String:Any]?)
    case getPictureDetails(pictureId:String)
    
    private var path: String {
        switch self {
        case .getPictures(params: let params):
            return "/photos\(params?.urlParamsString ?? "")"
        case .getPictureDetails(pictureId:let pictureId):
            return "/photos/\(pictureId)"
        }
    }
    
    var route: String {
        return Constants.apiHost + path
    }
}

extension Dictionary where Key == String {
    var urlParamsString:String {
        let dictionnaryJoined = self.map{ "\($0)=\($1)" }.joined(separator: "&")
        return "?\(dictionnaryJoined)"
    }
}
