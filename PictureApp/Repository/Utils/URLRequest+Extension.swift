//
//  URLRequest+Extension.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation

extension URLRequest {
    var curlDescription: String {
        var displayString = "curl -X \(self.httpMethod!)"
        if let absoluteUrl = self.url?.absoluteString {
            displayString += " '\(absoluteUrl)'"
        }
        if let allHTTPHeaderFields = self.allHTTPHeaderFields {
            let allHeadersKeys = Array(allHTTPHeaderFields.keys)
            let sortedHeadersKeys  = allHeadersKeys.sorted()
            for key in sortedHeadersKeys {
                displayString += " -H '\(key): \(self.value(forHTTPHeaderField: key)!)'"
            }
        }
        if self.httpMethod == "POST" ||
            self.httpMethod == "PUT" ||
            self.httpMethod == "PATCH", let body = self.httpBody {
            if let bodyString = String(data: body, encoding: String.Encoding.utf8) {
                displayString += " -d '\(bodyString)'"
            }
        }
        return displayString
    }
    
    public func showCurl() {
        #if !DEBUG
        return
        #endif
        var logString = String()
        logString.append("\n############## CURL ################")
        logString.append("#Request : [\n ")
        logString.append("\(self.curlDescription)")
        logString.append("\n]")
        print(logString)
    }
}
