//
//  NetworkError.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation
import UIKit

public protocol LocalizedDescriptionError: Error {
    var localizedDescription: String { get }
}

enum NetworkError: LocalizedDescriptionError, Equatable {
    case server
    case invalidJson
    case unknown
    
    var localizedDescription: String {
        return "\(self)"
    }
}

extension Error {
    var toNetworkError: NetworkError {
        if let mcerror = self as? NetworkError {
            return mcerror
        } else {
            return .unknown
        }
    }
}
