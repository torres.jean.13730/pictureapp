//
//  PictureAssembler.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation
import UIKit


class PictureAssembler {
    class func assemble() -> PicturesViewController {
        let viewController = PicturesViewController()
        viewController.viewModel = PictureViewModel(repository: PictureRepository.self)
        return viewController
    }
}
