//
//  PicturesViewModel.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation

protocol PictureViewModelProtocol {
    
    var uidata: [PictureUIData] { get set }
    
    var page: Int { get set }

    func getPictures()
    
    func refresh()
    
    func loadMore()
    
    var stateDidChanged: ((ViewModelStateEnum) -> Void)? { get set }
    
}

class PictureViewModel: PictureViewModelProtocol {
    
    // MARK: - variables
    private var state: ViewModelStateEnum = .unknown {
        didSet {
            DispatchQueue.main.async {
                self.stateDidChanged?(self.state)
            }
        }
    }
    
    var page = 1
    
    var stateDidChanged: ((ViewModelStateEnum) -> Void)?
    
    var uidata: [PictureUIData] = []
    
    var repository: PictureRepositoryProtocol.Type
    
    var converter: PictureConverterProtocol.Type = PictureConverter.self
    
    // MARK: - init
    init(repository: PictureRepositoryProtocol.Type) {
        self.repository = repository
    }
    
    // MARK: - Method
    func getPictures() {
        Task {
            self.state = .fetching
            do {
                let result = try await repository.getPictures(page: page)
                self.uidata += converter.convertPicturesToListPictureUIData(result)
                self.state = .fetched
            } catch {
                self.state = .error(error: error.toNetworkError)
            }
        } 
    }
    
    func refresh() {
        self.uidata.removeAll()
        page = 1
        self.getPictures()
    }
    
    func loadMore() {
        page += 1
        self.getPictures()
    }
}
