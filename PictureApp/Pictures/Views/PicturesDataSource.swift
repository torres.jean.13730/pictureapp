//
//  PicturesDataSource.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation
import UIKit

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension PicturesViewController: UICollectionViewDelegate,
                                  UICollectionViewDataSource,
                                  UICollectionViewDelegateFlowLayout {
    
    // MARK: UICollectionView numberOfItemsInSection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel?.uidata.count ?? 0
    }
    
    // MARK: UICollectionView cellForItemAt
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier: PictureCellIdentifier = isList ? .listView : .gridView
        var cell: UICollectionViewCell?
        switch identifier {
        case .listView:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: PictureCellIdentifier.listView.rawValue, for: indexPath) as? PictureListViewCell
            if indexPath.row < self.viewModel?.uidata.count ?? 0,
               let picture = self.viewModel?.uidata[indexPath.row] {
                (cell as? PictureListViewCell)?.setupCell(picture: picture)
            }
        case .gridView:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: PictureCellIdentifier.gridView.rawValue, for: indexPath) as? PictureGridViewCell
            if indexPath.row < self.viewModel?.uidata.count ?? 0,
                let picture = self.viewModel?.uidata[indexPath.row] {
                (cell as? PictureGridViewCell)?.setupCell(picture: picture)
            }
        }
        return cell ?? UICollectionViewCell()
    }
    
    // MARK: UICollectionViewLayout
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isList {
            return CGSize(width: collectionView.bounds.width - 10, height: 140)
        } else {
            return CGSize(width: (collectionView.bounds.width/3) - 5,
                          height: (collectionView.bounds.width/3) - 5)
        }
        
    }
    
    public func collectionView(_: UICollectionView,
                               layout _: UICollectionViewLayout,
                               minimumLineSpacingForSectionAt _: Int) -> CGFloat {
        7
    }

    public func collectionView(_: UICollectionView,
                               layout _: UICollectionViewLayout,
                               minimumInteritemSpacingForSectionAt _: Int) -> CGFloat {
        7
    }
    
    // MARK: UICollectionView willDisplay
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let itemCount = self.viewModel?.uidata.count, itemCount > 0, indexPath.row == itemCount - 1 && !self.isLoading {
            loadMoreData()
        }
    }

    // MARK: UICollectionView didSelectItemAt
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let pictureSelected = self.viewModel?.uidata[indexPath.row],
              let pictureId = pictureSelected.id else { return }
        self.coordinator?.goToPictureDetail(pictureId: pictureId)
    }
        
}

