//
//  PictureGridViewCell.swift
//  PictureApp
//
//  Created by Jean Torres on 25/02/2022.
//

import Foundation
import UIKit

class PictureGridViewCell: UICollectionViewCell {
    
    // MARK: - variables
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        imageView.image = "placeholder-image".imageFromName
        return imageView
    }()
    
    // MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - setupView
    func setupView() {
        contentView.addSubview(imageView)
    }
    
    // MARK: - setupConstraint
    func setupConstraint() {
        imageView.anchor(top: contentView.topAnchor,
                         leading: contentView.leadingAnchor,
                         bottom: contentView.bottomAnchor,
                         trailing: contentView.trailingAnchor)
    }
    
    // MARK: - setup cell
    func setupCell(picture: PictureUIData) {
        if let url = URL(string: picture.imageUrl ?? "") {
            imageView.load(url: url,
                           placeholder: "placeholder-image".imageFromName,
                           cache: nil)
        }
    }
}
