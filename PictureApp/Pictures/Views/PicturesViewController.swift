//
//  PicturesViewController.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation
import UIKit

class PicturesViewController: UIViewController {
    
    // MARK: - variables
    var viewModel: PictureViewModelProtocol? {
        didSet {
            self.viewModel?.stateDidChanged = handleState
            self.viewModel?.getPictures()
        }
    }
    
    let pullToRefresh = UIRefreshControl()
    
    var collectionView: UICollectionView!
    
    var coordinator: PicturesCoordinator?
    
    let layout = UICollectionViewFlowLayout()
    
    var isLoading = false
    
    var isList = true
    
    enum PictureCellIdentifier: String {
        case listView = "picture_list_view"
        case gridView = "picture_grid_view"
    }
    
    // MARK: - init
    public init(_ coder: NSCoder? = nil) {

        if let coder = coder {
            super.init(coder: coder)!
        } else {
            super.init(nibName: nil, bundle: nil)
        }
        self.setupView()
        self.setupConstraint()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
        self.setupConstraint()
    }

    // MARK: - setupView
    func setupView() {
        self.view.backgroundColor = .systemBackground
        self.title = "title_picture_list".localized
        displayLayoutButton(isList)
        self.pullToRefresh.addTarget(self, action: #selector(refreshFromPull), for: .valueChanged)
        layout.headerReferenceSize = CGSize(width: 0, height: 20)
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        self.collectionView.collectionViewLayout = layout
        self.collectionView.refreshControl = pullToRefresh
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(PictureListViewCell.self,
                                     forCellWithReuseIdentifier: PictureCellIdentifier.listView.rawValue)
        self.collectionView.register(PictureGridViewCell.self,
                                     forCellWithReuseIdentifier: PictureCellIdentifier.gridView.rawValue)
        
        self.view.addSubview(collectionView)
    }
    
    func displayLayoutButton(_ isList:Bool) {
        let infoButton = UIBarButtonItem(title: isList ? "list".localized : "grid".localized,
                                         style: .done,
                                         target: self,
                                         action: #selector(changeLayoutCollection))
        self.navigationItem.rightBarButtonItem = infoButton
    }
    
    func displayEmptyViewIfNeeded() {
        if self.viewModel?.uidata.count == 0 {
            self.collectionView.setEmptyView("picture_list_empty_message".localized)
        } else {
            self.collectionView.restore()
        }
    }
    
    // MARK: - setupConstraint
    func setupConstraint() {
        collectionView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
               leading: view.safeAreaLayoutGuide.leadingAnchor,
               bottom: view.bottomAnchor,
               trailing: view.safeAreaLayoutGuide.trailingAnchor)
    }
    
    // MARK: - Method
    func refreshData() {
        if !self.isLoading {
            self.viewModel?.refresh()
        }
    }
    
    func loadMoreData() {
        if !self.isLoading {
            self.viewModel?.loadMore()
        }
    }
    
}

// MARK: - action
extension PicturesViewController {
    @objc func refreshFromPull() {
        if !self.isLoading {
            refreshData()
        }
    }
    
    @objc func changeLayoutCollection() {
        isList = !isList
        displayLayoutButton(isList)
        self.collectionView.reloadData()
        
    }
}

// MARK: - handleState
extension PicturesViewController {
    private func handleState(_ state: ViewModelStateEnum) {
        self.pullToRefresh.endRefreshing()
        switch state {
        case .fetching:
            self.isLoading = true
            self.pullToRefresh.beginRefreshing()
            break
        case .fetched:
            displayEmptyViewIfNeeded()
            self.collectionView.reloadData()
            self.isLoading = false
        case .error(error: let error):
            #if DEBUG
            print("###### Error Get Picture List #######")
            print(error.localizedDescription)
            #endif
            displayEmptyViewIfNeeded()
            self.collectionView.reloadData()
            self.displayError(message: error.localizedDescription.localized)
        default:
            break
        }
    }
}
