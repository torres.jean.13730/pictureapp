//
//  PictureListViewCell.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation
import UIKit

class PictureListViewCell: UICollectionViewCell {
    
    // MARK: - variables
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        imageView.image = "placeholder-image".imageFromName
        return imageView
    }()
    
    let author: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        return label
    }()
    
    let describe: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.numberOfLines = 5
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.textColor = UIColor.systemGray
        return label
    }()
    
    let location: UILabel = {
        let label = UILabel()
        label.font = UIFont.italicSystemFont(ofSize: 11)
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.textColor = UIColor.systemGray
        return label
    }()
    
    // MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - setupView
    func setupView() {
        contentView.addSubview(imageView)
        contentView.addSubview(author)
        contentView.addSubview(describe)
        contentView.addSubview(location)
    }
    
    // MARK: - setupConstraint
    func setupConstraint() {
        imageView.anchor(top: contentView.topAnchor,
                         leading: contentView.leadingAnchor,
                         bottom: contentView.bottomAnchor,
                         trailing: nil,
                         padding: .init(top: 1, left: 0, bottom: 5, right: 0))
        
        imageView.anchorHeightEgalWith()
        
        author.anchor(top: imageView.topAnchor,
                      leading: imageView.trailingAnchor,
                      bottom: nil,
                      trailing: contentView.trailingAnchor,
                      padding: .init(top: 2, left: 10, bottom: 0, right: 0))
        
        describe.anchor(top: author.bottomAnchor,
                      leading: imageView.trailingAnchor,
                      bottom: nil,
                      trailing: contentView.trailingAnchor,
                      padding: .init(top: 0, left: 10, bottom: 0, right: 0))
        
        location.anchor(top: describe.bottomAnchor,
                        leading: imageView.trailingAnchor,
                        bottom: nil,
                        trailing: contentView.trailingAnchor,
                        bottomLess: contentView.bottomAnchor,
                        padding: .init(top: 5, left: 10, bottom: 3, right: 0))
    }
    
    // MARK: - setup cell
    func setupCell(picture: PictureUIData) {
        if let url = URL(string: picture.imageUrl ?? "") {
            imageView.load(url: url,
                           placeholder: "placeholder-image".imageFromName,
                           cache: nil)
        }
        author.text = picture.author
        describe.text = picture.describe
        location.text = picture.location
    }
}



