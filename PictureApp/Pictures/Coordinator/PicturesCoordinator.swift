//
//  PicturesCoordinator.swift
//  PictureApp
//
//  Created by Jean Torres on 21/02/2022.
//

import Foundation
import UIKit

protocol PicturesCoordinatorProtocol: Coordinator {
    func goToPictureDetail(pictureId:String)
}

class PicturesCoordinator: PicturesCoordinatorProtocol {
    
    var navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewController = PictureAssembler.assemble()
        viewController.coordinator = self
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    func goToPictureDetail(pictureId:String) {
        let coordinator = PictureDetailsCoordinator(navigationController: navigationController,
                                                    pictureId: pictureId)
        coordinator.start()
        
    }
    
}
