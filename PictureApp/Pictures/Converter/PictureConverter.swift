//
//  PictureConverter.swift
//  PictureApp
//
//  Created by Jean Torres on 22/02/2022.
//

import Foundation

protocol PictureConverterProtocol {
    static func convertPicturesToListPictureUIData(_ output:[Picture]) -> [PictureUIData]
}

class PictureConverter: PictureConverterProtocol {
    
    /// convert a Pictures list output to PictureUIData list
    /// - Parameter output: [Picture]
    /// - Returns: [PictureUIData]
    static func convertPicturesToListPictureUIData(_ output:[Picture]) -> [PictureUIData] {
        return output.map({convertPictureToPictureUIData($0)})
    }
    
    
    /// converter a Picture output to PictureUIData
    /// - Parameter output: Picture
    /// - Returns: PictureUIData
    private static func convertPictureToPictureUIData(_ output:Picture) -> PictureUIData {
        var uidata = PictureUIData()
        uidata.id = output.id
        uidata.author = output.user?.name
        uidata.describe = output.description
        uidata.imageUrl = output.urls?.small
        uidata.location = output.user?.location
        return uidata
    }
}
