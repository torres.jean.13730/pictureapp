//
//  PictureUIData.swift
//  PictureApp
//
//  Created by Jean Torres on 22/02/2022.
//

import Foundation


struct PictureUIData {
    
    var id: String?
    
    var author: String?
    
    var describe: String?
    
    var imageUrl:String?
    
    var location:String?

}
