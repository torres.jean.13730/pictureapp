# Sommary

[TOC]

# PictureApp

![](./preview/iconApp.png)

PictureApp is an application that displays a list of photos with the artist's name, a description and the location of the photo.

You can display the photos either in a list or in a grid.

When you click on the photo, the photo is displayed in full screen with the possibility of displaying the exif information of the photo.

![](./preview/picture1.png) ![](./preview/picture2.png) ![](./preview/picture3.png)

## Install
This application don't use tiers library.
you just need to run the application

## Environment 
It's an app for ios 15, written in swift 5.5 on xcode 13.2.1

## Architecture  

###  **MVVM**
![](https://upload.wikimedia.org/wikipedia/commons/8/87/MVVMPattern.png)

Model–view–viewmodel (MVVM) is a software architectural pattern that facilitates the separation of the development of the graphical user interface (the view) – be it via a markup language or GUI code – from the development of the business logic or back-end logic (the model) so that the view is not dependent on any specific model platform. The viewmodel of MVVM is a value converter,[1] meaning the viewmodel is responsible for exposing (converting) the data objects from the model in such a way that objects are easily managed and presented. In this respect, the viewmodel is more model than view, and handles most if not all of the view's display logic.[1] The viewmodel may implement a mediator pattern, organizing access to the back-end logic around the set of use cases supported by the view.

###  **Async await**
Description: Swift now supports asynchronous functions — a pattern commonly known as async/await. Discover how the new syntax can make your code easier to read and understand. Learn what happens when a function suspends, and find out how to adapt existing completion handlers to asynchronous functions.
https://www.wwdcnotes.com/notes/wwdc21/10132/
###  **Object Codable**
Make your data types encodable and decodable for compatibility with external representations such as JSON
https://developer.apple.com/documentation/foundation/archives_and_serialization/encoding_and_decoding_custom_types
### Consigne

**Linxo Coding Exercise**
We'd like for you to build a simple photo app which displays a set of photos using the Unsplash 
API.

We've already setup an account for you, so you can use the Unsplash API by simply including the 
following header:

Authorization: Client-ID 86a58c98b5095e4e069222d9caaf57e27a4c0028772b5a76e4f42ff9a53ab563

#####The App 
Being a photo app, the following features need to be implemented:

1. Display a feed of photos: for each photo you must display the artist's name and description.
2. When tapping on a given photo, it should be displayed full screen with a button for additional 
information.
3. When tapping on the button, the photo's EXIF data (available through the API) should be shown.

##### Suggested Improvements
• Lazily load the data for a user experience that is as smooth as possible.
• In the feed, add a toggle for displaying the images in a grid or a list layout.
• Add some kind of a caching mechanism to display the images faster.
• When available, add the location information to the photo's additional information.

We love kickass UX (responsive design, animations) and there's a ton of other information available 
in the API, so go nuts!

##### Notes
• You are free to use either Swift, Objective-C or SwiftUl, whichever you feel more comfortable 
with.
• While not required, you are welcome to use code from third-party libraries (except any Unsplash 
API wrapper).
• We're not judging your artistic skills, using basic UIKit components is fine.
• You will be delivering your project on a git repository, or alternatively the zipped project by 
email.

##### Evaluation
• It goes without saying, but your app must compile. If it requires additional steps in order to 
build, they must be specified in the project.
• We’ll be looking at the quality of your code through its style, consistency, architecture, 
design, and use of best practices.
• Implementing unit tests is a plus.
